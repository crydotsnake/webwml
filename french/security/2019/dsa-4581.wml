#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans git, un système de
gestion de versions distribué, rapide et évolutif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

<p>Il a été signalé que l'option --export-marks de fast-import de git est
aussi exposée au moyen de la fonctionnalité export-marks=... de la commande
in-stream, permettant l'écrasement de chemins arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

<p>Les noms des sous modules ne sont pas validés de façon suffisamment
stricte, permettant des attaques très ciblées au moyen de l'exécution
distante de code lors de la réalisation de clones récursifs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

<p>Joern Schneeweisz a signalé une vulnérabilité où un clone récursif suivi
de la mise à jour d'un sous-module pourrait exécuter du code contenu par le
dépôt sans que l'utilisateur ne soit explicitement interrogé à ce sujet. Il
est désormais interdit aux « .gitmodule » d'avoir des entrées qui définissent
« submodule.&lt;name&gt;.update=!command ».</p></li>

</ul>

<p>En complément, cette mise à jour corrige un certain nombre de problèmes
de sécurité qui existent seulement si git fonctionne sur un système de
fichiers NTFS (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>
<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 1:2.11.0-3+deb9u5.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:2.20.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
# $Id: $
