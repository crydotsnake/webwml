#use wml::debian::translation-check translation="10d6441f58b80849b1c23b3599dafc9a773a04f2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Joe Vennix a découvert une vulnérabilité de dépassement de pile dans
sudo, un programme conçu pour donner des droits limités de superutilisateur
à des utilisateurs particuliers, déclenchable lorsque sudo est configuré
avec l'option <q>pwfeedback</q> activée. Un utilisateur non privilégié
peut tirer avantage de ce défaut pour obtenir les privilèges complets du
superutilisateur.</p>

<p>Plus de détails sont disponibles dans l'annonce amont à l'adresse
<a href="https://www.sudo.ws/alerts/pwfeedback.html">https://www.sudo.ws/alerts/pwfeedback.html</a> .</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 1.8.19p1-2.1+deb9u2.</p>

<p>Pour la distribution stable (Buster), l'exploitation du bogue est
empêchée du fait d'une modification du traitement de fin de fichier (EOF)
introduite dans la version 1.8.26.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sudo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sudo">\
https://security-tracker.debian.org/tracker/sudo</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4614.data"
# $Id: $
