#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.10</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la dixième mise à jour de sa
distribution oldstable Debian <release> (codename <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version oldstable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction base-files "Mise à jour pour cette version ; ajout de VERSION_CODENAME à os-release">
<correction basez "Décodage correct des chaînes encodées avec base64url">
<correction biomaj-watcher "Correction des mises à niveau de Jessie à Stretch">
<correction c-icap-modules "Ajout de la prise en charge de clamav 0.101.1">
<correction chaosreader "Ajout de la dépendance manquante à libnet-dns-perl">
<correction clamav "Nouvelle version amont stable : ajout de limite de temps de balayage pour réduire le risque de « bombes zip » [CVE-2019-12625] ; correction d'écriture hors limites dans la bibliothèque NSIS bzip2 [CVE-2019-12900]">
<correction corekeeper "Pas d'utilisation de /var/crash accessible à tous en écriture avec le script de sauvegarde ; gestion des versions antérieures du noyau Linux d'une façon plus sûre ; pas de troncature des noms principaux des exécutables avec des espaces">
<correction cups "Correction de multiples problèmes de sécurité ou de divulgation – problèmes de dépassements de tampon SNMP [CVE-2019-8696 CVE-2019-8675], de dépassement de tampon IPP, de déni de service et de divulgation de mémoire dans l'ordonnanceur">
<correction dansguardian "Ajout de la prise en charge de clamav 0.101.1">
<correction dar "Reconstruction pour mettre à jour les paquets <q>built-using</q>">
<correction debian-archive-keyring "Ajout des clés de Buster ; retrait des clés de Wheezy">
<correction fence-agents "Correction d'un problème de déni de service [CVE-2019-10153]">
<correction fig2dev "Plus d'erreur de segmentation sur les pointes de flèche circulaires ou semi-circulaires avec un grossissement supérieur à 42 [CVE-2019-14275]">
<correction fribidi "Correction de sortie de droite à gauche dans le mode texte de l'installateur Debian">
<correction fusiondirectory "Vérifications plus strictes des recherches LDAP ; ajout de la dépendance manquante à php-xml">
<correction gettext "Plantage de xgettext() empêché lors d'une exécution avec l'option --its=FILE">
<correction glib2.0 "Création de répertoire et de fichier avec des droits restreints lors de l'utilisation de GKeyfileSettingsBackend [CVE-2019-13012] ; plus de débordement de tampon en lecture lors du formatage des messages d'erreur pour un codage UTF-8 non valable dans GMarkup [CVE-2018-16429] ; plus de déréférencement de pointeur NULL lors de l'analyse de GMarkup non valable avec une balise fermante mal formée pas appairée avec une balise ouvrante [CVE-2018-16429]">
<correction gocode "gocode-auto-complete-el : pré-dépendance à auto-complete-el versionné pour corriger les mises à niveau de Jessie à Stretch">
<correction groonga "Réduction du risque d'élévation des privilèges en changeant le propriétaire et le groupe des logs avec l'option <q>su</q>">
<correction grub2 "Corrections pour la prise en charge de l'UEFI de Xen">
<correction gsoap "Correction d'un problème de déni de service si une application serveur est construite avec l'option -DWITH_COOKIES [CVE-2019-7659] ; correction d'un problème avec le receveur du protocole DIME et d'en-têtes DIME mal formés">
<correction gthumb "Correction d'un bogue de double libération de mémoire [CVE-2018-18718]">
<correction havp "Ajout de la prise en charge de clamav 0.101.1">
<correction icu "Correction d'une erreur de segmentation dans la commande pkgdata">
<correction koji "Correction de problème d'injection SQL [CVE-2018-1002161] ; validation correcte de chemins SCM [CVE-2017-1002153]">
<correction lemonldap-ng "Correction d'un régression d'authentification interdomaine ; correction d'une vulnérabilité d'entité externe XML">
<correction libcaca "Correction de problèmes de dépassement d'entier [CVE-2018-20545 CVE-2018-20546 CVE-2018-20547 CVE-2018-20548 CVE-2018-20549]">
<correction libclamunrar "Nouvelle version amont stable">
<correction libconvert-units-perl "Reconstruction sans modification avec un numéro de version corrigé">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libebml "Application de corrections amont pour des lectures hors limites de tampon de tas">
<correction libevent-rpc-perl "Correction d'échec de construction dû à des certificats SSL de test expirés">
<correction libgd2 "Correction de lecture non initialisée dans gdImageCreateFromXbm [CVE-2019-11038]">
<correction libgovirt "Nouvelle génération des certificats de test avec une date d'expiration loin dans le futur pour éviter l'échec des tests">
<correction librecad "Correction de déni de service au moyen d'un fichier contrefait [CVE-2018-19105]">
<correction libsdl2-image "Correction de plusieurs problèmes de sécurité">
<correction libthrift-java "Correction de contournement de négociation SASL [CVE-2018-1320]">
<correction libtk-img "Arrêt de l'utilisation de copies internes des codecs JPEG, Zlib et PixarLog, corrigeant des plantages">
<correction libu2f-host "Correction de fuite de mémoire de pile [CVE-2019-9578]">
<correction libxslt "Correction de contournement de l'infrastructure de sécurité [CVE-2019-11068], lecture non initialisée de jeton xsl:number [CVE-2019-13117] et lecture non initialisée de caractères groupés UTF-8 [CVE-2019-13118]">
<correction linux "Nouvelle version amont avec ABI modifiée ; corrections de sécurité [CVE-2015-8553 CVE-2017-5967 CVE-2018-20509 CVE-2018-20510 CVE-2018-20836 CVE-2018-5995 CVE-2019-11487 CVE-2019-3882]">
<correction linux-latest "Mise à jour pour l'ABI du noyau 4.19.0-6">
<correction liquidsoap "Correction de la compilation avec Ocaml 4.02">
<correction llvm-toolchain-7 "Nouveau paquet pour prendre en charge la construction des nouvelles versions de Firefox">
<correction mariadb-10.1 "Nouvelle version amont stable ; corrections de sécurité [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2805 CVE-2019-2627 CVE-2019-2614]">
<correction minissdpd "Évitement de vulnérabilité d'utilisation de mémoire après libération qui pourrait permettre à un attaquant distant de planter le processus [CVE-2019-12106]">
<correction miniupnpd "Correction de problèmes de déni de service [CVE-2019-12108 CVE-2019-12109 CVE-2019-12110] ; correction de fuite d'information [CVE-2019-12107]">
<correction mitmproxy "Mise en liste noire des tests qui demandent un accès à Internet ; pas d'insertion de dépendances à version supérieure non désirées">
<correction monkeysphere "Correction d'échec de construction en mettant à jour les tests pour adapter une version de GnuPG mise à jour dans Stretch produisant une sortie différente">
<correction nasm-mozilla "Nouveau paquet pour prendre en charge la construction des nouvelles versions de Firefox">
<correction ncbi-tools6 "Réempaquetage sans les données non libres d’UniVec.*">
<correction node-growl "Nettoyage de l'entrée avant de la transmettre à exec">
<correction node-ws "Limitation de la taille d'envoi [CVE-2016-10542]">
<correction open-vm-tools "Correction d'un possible problème de sécurité avec les droits du répertoire et du chemin de transit intermédiaire">
<correction openldap "Limitation de proxyauthz de rootDN à ses propres bases de données [CVE-2019-13057] ; déclaration d'ACL sasl_ssf exigée à chaque connexion [CVE-2019-13565] ; correction de slapo-rwm pour ne pas vider le filtre original quand le filtre réécrit n'est pas valable">
<correction openssh "Correction d'un blocage dans la recherche de correspondance de clé">
<correction passwordsafe "Pas d'installation des fichiers de localisation dans un sous-répertoire supplémentaire">
<correction pound "Correction de dissimulation de requête au moyen d'en-têtes contrefaits [CVE-2016-10711]">
<correction prelink "Reconstruction pour mettre à jour les paquets <q>built-using</q>">
<correction python-clamav "Ajout de la prise en charge de clamav 0.101.1">
<correction reportbug "Mise à jour des noms de version, suite à la publication de Buster">
<correction resiprocate "Résolution d'un problème d'installation avec libssl-dev et --install-recommends">
<correction sash "Reconstruction pour mettre à jour les paquets <q>built-using</q>">
<correction sdl-image1.2 "Correction des dépassements de tampon [CVE-2018-3977 CVE-2019-5058 CVE-2019-5052] et d'accès hors limites [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction signing-party "Correction d'appel shell non sûr permettant l'injection de commande au moyen de l'identifiant d'un utilisateur [CVE-2019-11627]">
<correction slurm-llnl "Correction de dépassement de tas dans les systèmes 32 bits [CVE-2019-6438]">
<correction sox "Correction de plusieurs problèmes de sécurité [CVE-2019-8354 CVE-2019-8355 CVE-2019-8356 CVE-2019-8357 927906 CVE-2019-1010004 CVE-2017-18189 881121 CVE-2017-15642 882144 CVE-2017-15372 878808 CVE-2017-15371 878809 CVE-2017-15370 878810 CVE-2017-11359 CVE-2017-11358 CVE-2017-11332">
<correction systemd "Plus d'arrêt du client ndisc en cas d'erreur de configuration">
<correction t-digest "Reconstruction sans modification pour éviter la réutilisation de la version 3.0-1 pré-epoch">
<correction tenshi "Correction d'un problème de fichier PID qui permet aux utilisateurs locaux de tuer des processus arbitraires [CVE-2017-11746]">
<correction tzdata "Nouvelle version amont">
<correction unzip "Correction d'analyses incorrectes de valeurs 64 bits dans fileio.c ; correction de problèmes de « bombe zip » [CVE-2019-13232]">
<correction usbutils "Mise à jour de la liste des identifiants USB">
<correction xymon "Correction de plusieurs problèmes de sécurité (serveur uniquement) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubico-piv-tool "Correction de problèmes de sécurité [CVE-2018-14779 CVE-2018-14780]">
<correction z3 "Pas de configuration du SONAME de libz3java.so à libz3.so.4">
<correction zfs-auto-snapshot "Arrêt silencieux des tâches cron après la suppression d'un paquet">
<correction zsh "Reconstruction pour mettre à jour les paquets <q>built-using</q>">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4435 libpng1.6>
<dsa 2019 4436 imagemagick>
<dsa 2019 4437 gst-plugins-base1.0>
<dsa 2019 4438 atftp>
<dsa 2019 4439 postgresql-9.6>
<dsa 2019 4440 bind9>
<dsa 2019 4441 symfony>
<dsa 2019 4442 cups-filters>
<dsa 2019 4442 ghostscript>
<dsa 2019 4443 samba>
<dsa 2019 4444 linux>
<dsa 2019 4445 drupal7>
<dsa 2019 4446 lemonldap-ng>
<dsa 2019 4447 intel-microcode>
<dsa 2019 4448 firefox-esr>
<dsa 2019 4449 ffmpeg>
<dsa 2019 4450 wpa>
<dsa 2019 4451 thunderbird>
<dsa 2019 4452 jackson-databind>
<dsa 2019 4453 openjdk-8>
<dsa 2019 4454 qemu>
<dsa 2019 4455 heimdal>
<dsa 2019 4456 exim4>
<dsa 2019 4457 evolution>
<dsa 2019 4458 cyrus-imapd>
<dsa 2019 4459 vlc>
<dsa 2019 4460 mediawiki>
<dsa 2019 4461 zookeeper>
<dsa 2019 4462 dbus>
<dsa 2019 4463 znc>
<dsa 2019 4464 thunderbird>
<dsa 2019 4465 linux>
<dsa 2019 4466 firefox-esr>
<dsa 2019 4467 vim>
<dsa 2019 4468 php-horde-form>
<dsa 2019 4469 libvirt>
<dsa 2019 4470 pdns>
<dsa 2019 4471 thunderbird>
<dsa 2019 4472 expat>
<dsa 2019 4473 rdesktop>
<dsa 2019 4475 openssl>
<dsa 2019 4475 openssl1.0>
<dsa 2019 4476 python-django>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4485 openjdk-8>
<dsa 2019 4487 neovim>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4492 postgresql-9.6>
<dsa 2019 4494 kconfig>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4506 qemu>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction pump "Non maintenu ; problèmes de sécurité">
<correction teeworlds "Problèmes de sécurité ; incompatible avec les serveurs actuels">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de oldstable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution oldstable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
