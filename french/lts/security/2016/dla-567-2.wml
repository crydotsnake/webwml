#use wml::debian::translation-check translation="0e9af03d8cf68ff5ddec5d25a056c8dacce03437" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de données
MySQL. Les vulnérabilités sont corrigées en mettant MySQL à niveau vers la
nouvelle version amont 5.5.50. Veuillez consulter les notes de publication
de MySQL 5.5 et les annonces de mises à jour critiques d'Oracle pour de plus
amples détails :</p>

<ul>
<li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-50.html">\
https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-50.html</a></li>
<li><a href="http://www.oracle.com/technetwork/security-advisory/cpujul2016-2881720.html">\
http://www.oracle.com/technetwork/security-advisory/cpujul2016-2881720.html</a></li>
</ul>


<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.5.50-0+deb7u2.</p>

<p>Note de régression : j'ai construit à tort l'envoi précédent
de 5.5.50-0+deb7u1 avec l'empaquetage de Debian jessie-security. Bien
qu'aucun problème n'ait été identifié sur amd64, j'ai envoyé une nouvelle
version construite avec l'empaquetage normal de Wheezy.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-567-2.data"
# $Id: $
