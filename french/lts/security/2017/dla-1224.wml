#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans le système de contrôle de versions
Mercurial qui pourrait conduire à l’exécution distante de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

<p>Un dépôt Mercurial spécialement contrefait pourrait faire que des sous-dépôts
Git exécutent du code arbitraire sous la forme d’un script
.git/hooks/post-update consulté dans le répertoire parent. L’usage habituel de
Mercurial empêche la construction de tels répertoires, mas ils peuvent être
créés par programmation.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.2-4+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mercurial.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1224.data"
# $Id: $
