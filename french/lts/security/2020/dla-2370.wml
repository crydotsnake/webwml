#use wml::debian::translation-check translation="345011ff7ec2ffdbb97d4538785c24a1a4bfafc0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque par traversée de répertoires
dans pip, l’installateur de paquets Python.</p>

<p>Quand une URL était donnée dans une commande d’installation, tel qu’un en-tête
<tt>Content-Disposition</tt> pouvant avoir des composants <tt>../</tt> dans leur
nom de fichier, des fichiers locaux arbitraires (par exemple,
<tt>/root/.ssh/authorized_keys</tt>) pourraient être écrasés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20916">CVE-2019-20916</a>

<p>Le paquet pip avant sa version 19.2 pour Python permet une traversée de
répertoires quand une URL est indiquée dans une commande d’installation, car un
en-tête Content-Disposition peut avoir ../ dans un nom de fichier, comme le
montre l’écrasement du fichier /root/.ssh/authorized_keys. Cela se produit dans
_download_http_url dans _internal/download.py.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 9.0.1-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-pip.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2370.data"
# $Id: $
