#use wml::debian::translation-check translation="abd26a756ed3aa3835bfaf0e41f2066aaf78e122" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème mineur de sécurité et un bogue sévère d’empaquetage ont été
corrigés dans tinyproxy, un démon léger de mandataire HTTP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11747">CVE-2017-11747</a>

<p>main.c dans Tinyproxy créait un fichier /var/run/tinyproxy/tinyproxy.pid
en baissant les privilèges à ceux d’un compte non superutilisateur. Cela pouvait
permettre à des utilisateurs locaux de tuer des processus arbitraires en
exploitant l'accès à ce compte non superutilisateur pour modifier tinyproxy.pid
avant qu’un script de superutilisateur exécute une commande
<q>kill `cat /run/tinyproxy/tinyproxy.pid`</q>.</p></li>

<li><p>Autre problème</p>

<p>Un défaut grave a été découvert par Tim Duesterhus dans le script init de
Debian pour tinyproxy. Avec un fichier de configuration tiny.conf dont l’option
PidFile est supprimée, la prochaine exécution de logrotate (si installé) pouvait
modifier le propriétaire du répertoire de base du système (« / »)
à tinyproxy:tinyproxy.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.8.3-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets  mandataire tin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2163.data"
# $Id: $
