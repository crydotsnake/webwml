#use wml::debian::translation-check translation="c2e55ca5935bbeeda8ca377a8a7489adecbaec54" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La prise en charge du serveur http2 dans ce paquet était vulnérable à
certains types d’attaque par déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9512">CVE-2019-9512</a>

<p>Ce code était vulnérable à des attaques par <q>ping flood</q>, conduisant
éventuellement à un déni de service. L’attaquant envoie continuellement des
<q>ping</q> à un pair HTTP/2, obligeant le pair à bâtir une file d'attente interne de
réponses. Selon l’efficacité de cette mise en file d'attente des données, cela peut
provoquer une consommation excessive de CPU ou de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9514">CVE-2019-9514</a>

<p>Ce code était vulnérable à une réinitialisation d’envois, conduisant
éventuellement à un déni de service. L’attaquant ouvre un certain nombre de
flux et envoie une requête non valable sur chaque flux qui sollicite un flux
de trames RST_STREAM du pair. Selon la façon dont celles-ci sont gérées, cela
peut provoquer une consommation excessive de CPU ou de mémoire.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:0.0+git20161013.8b4af36+dfsg-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-golang-x-net-dev.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-golang-x-net-dev, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-golang-x-net-dev">https://security-tracker.debian.org/tracker/golang-golang-x-net-dev</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2485.data"
# $Id: $
