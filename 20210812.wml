ine-tag pagetitle>Das Debian-Projekt betrauert den Verlust von Robert Lemmen, Karl Ramm und Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="a48c50faef33185d0ae2746854bc99435817bad1" maintainer="Erik Pfannenstein"
# $Id$

<p>
Das Debian-Projekt hat während des letzten Jahres mehrere Mitglieder seiner Gemeinschaft
verloren.
</p>

<p>
Im Juni 2020 verschied Robert Lemmen nach einer ernsten Krankheit. Robert war
seit den frühen 2000ern regelmäßiger Teilnehmer der Münchner Debian-Treffen
und hat vor Ort bei Ausstellungsständen mitgeholfen.
Im Jahr 2007 wurde er zum Debian-Entwickler und hat seitdem mehrere Beiträge
geleistet, unter anderem paketierte er Module für Raku (zu seiner Zeit Perl6)
und half anderen Mitwirkenden, sich ins Raku-Team einzubringen.
Außerdem hat er einige Arbeit in das Nachverfolgen zirkulärer Abhängigkeiten
in Debian gesteckt.
</p>

<p>
Karl Ramm starb im Juni 2020 nach Komplikationen, die von metastasierendem
Darmkrebs verursacht wurden. Er war seit 2001 Debian-Entwickler und hat mehrere
Komponenten für das
<a href="https://de.wikipedia.org/wiki/Projekt_Athena">Projekt Athena</a> des MIT
paketiert.
Er beschäftigte sich leidenschaftlich gern mit Technik und Debian und war immer
daran interessiert, anderen im Entdecken und Voranbringen ihrer Leidenschaften
zu assistieren.
</p>

<p>
Im April 2021 ging Rogério Theodoro de Brito wegen der COVID-19-Pandemie von uns.
Rogério schrieb gerne kleine Werkzeuge und trug schon seit über 15 Jahren zu
Debian bei. Mit seinen Beiträgen half er dabei, Kurobox/Linkstation-Geräte in
Debian nutzbar zu machen und er betreute das youtube-dl-Werkzeug. Darüber hinaus
beteiligte er sich auch an mehreren Originalautor-Projekten und war dort
<q>Debian-Kontakt</q>.
</p>

<p>
Das Debian-Projekt ehrt Robert, Karl und Rogério für ihre gute Arbeit sowie ihr
starkes Engagement für Debian und Freie Software. Ihre Beiträge werden nicht
vergessen werden und das hohe Niveau ihrer Beiträge werden anderen als
Inspiration dienen.
</p>


<h2>Über Debian</h2>

<p>
Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software,
die ihre Zeit und Bemühungen einbringen, um das vollständig freie
Betriebssystem Debian zu erschaffen.
</p>

<h2>Kontaktinformationen</h2>

<p>
Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a> oder schicken eine E-Mail (auf
Englisch) an &lt;press@debian.org&gt;.
</p>
