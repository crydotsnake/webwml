#use wml::debian::translation-check translation="7d4e416de9b0d5870d3b56d250bdbed4f5cdde8b"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

    <p>Se informó de un defecto en el código del sistema de archivos JFS que permite que un atacante
    local con capacidad para establecer atributos extendidos provoque
    denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

    <p>Adam <q>pi3</q> Zabrocki informó de un defecto de «uso tras liberar» en la lógica
    de redimensionamiento del área circular de memoria («ring buffer») de ftrace debido a una condición de carrera, que podría
    dar lugar a denegación de servicio o a fuga de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27830">CVE-2020-27830</a>

    <p>Shisong Qin informó de un defecto de desreferencia de puntero NULL en el controlador
    central del lector de pantalla Speakup.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

    <p>David Disseldorp descubrió que la implementación de los dispositivos («targets») SCSI LIO
    no realizaba suficientes comprobaciones en algunas peticiones XCOPY. Un
    atacante con acceso a un LUN y que conozca las asignaciones de números de serie
    de unidades («Unit Serial Number») puede aprovecharse de este defecto para leer de y escribir en cualquier
    «backstore» LIO, independientemente de la configuración de transporte SCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568 (XSA-349)</a>

    <p>Michael Kurth y Pawel Wieczorkiewicz informaron de que las interfaces de usuario («frontends») pueden
    desencadenar el agotamiento de la memoria (OOM, por sus siglas en inglés) en backends mediante la actualización de una ruta monitorizada («watched path»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569 (XSA-350)</a>

    <p>Olivier Benjamin y Pawel Wieczorkiewicz informaron de un defecto de «uso tras
    liberar» que puede ser desencadenado por una interfaz de bloque («block frontend») en Linux blkback. Un
    huésped con mal comportamiento puede desencadenar una caída de dom0 conectando y
    desconectando continuamente una interfaz de bloque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

    <p>Jann Horn informó de un problema de inconsistencia de serialización en el subsistema
    tty que puede permitir que un atacante local lleve a cabo un
    ataque de «lectura tras liberar» contra TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

    <p>Jann Horn informó de un problema de serialización en el subsistema tty que puede
    dar lugar a un «uso tras liberar». Un atacante local puede aprovecharse de
    este defecto para provocar corrupción de memoria o para obtener elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

    <p>Se descubrió un defecto de desbordamiento de memoria en el controlador WiFi mwifiex
    que podría dar lugar a denegación de servicio o a ejecución de
    código arbitrario mediante un SSID largo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

    <p>Se descubrió que los PI futexes contienen un «uso tras liberar» en la pila del núcleo
    durante la gestión de errores. Un usuario sin privilegios podría utilizar este defecto para
    provocar la caída del núcleo (dando lugar a denegación de servicio) o para obtener elevación
    de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20177">CVE-2021-20177</a>

    <p>Se descubrió un defecto en la implementación Linux de búsqueda de coincidencias de cadenas
    en paquetes. Un usuario con privilegios (de root o con CAP_NET_ADMIN) puede
    aprovecharse de este defecto para provocar un panic del núcleo al insertar
    reglas de iptables.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.171-2.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4843.data"
