#use wml::debian::template title="Informações para consultores(as) Debian"
#use wml::debian::translation-check translation="b27d50a764c00727d11d19b785f566eff3b2fa87"

<h2>Política para a página de consultores(as) Debian</h2>

<p>Se você deseja ser listado(a) como um(a) consultor(a) no site web do Debian,
observe as seguintes regras:</p>

<ul>
  <li>Informações de contato obrigatórias<br />
    Você deve fornecer um endereço de e-mail válido e responder aos e-mails
    enviados a você em no máximo quatro semanas. A fim de evitar abusos,
    quaisquer solicitações (acréscimos, remoções ou alterações) devem ser
    enviadas desse mesmo endereço. Para evitar spam, você pode solicitar que seu
    endereço de e-mail seja codificado (por exemplo, <q>debian -dot- consulting
    -at- example -dot- com </q>). Se você preferir assim, indique explicitamente
    no seu envio. Alternativamente, você também pode solicitar que seu endereço
    de e-mail não seja visível na página web (embora precisemos de um endereço
    de e-mail válido para a manutenção da lista).
    Se quiser que seu endereço fique oculto na lista, você pode fornecer a URL
    de um formulário de contato em seu site web para que possamos criar um link.
    <q>Contato</q> deve ser usado para isso.
  </li>
  <li>Se você fornecer um link para um site web, esse site web deve mencionar
    os seus serviços de consultoria Debian. Fornecer o link direto em vez de
    apenas a página inicial não é obrigatório se a informação for razoavelmente
    acessível, além de muito apreciada.
  </li>
  <li>Várias cidades/regiões/países<br />
    Você deve escolher o país (apenas um) no qual deseja ser listado. Cidades,
    regiões ou países adicionais devem ser listados como parte das informações
    adicionais ou em seu próprio site web.
  </li>
  <li>Regras para desenvolvedores(as) Debian <br />
    Você não tem permissão para usar seu endereço oficial @debian.org para a
    lista de consultores(as).<br />
    Se você gostaria de fornecer uma URL, você não pode usar um domínio oficial
    *.debian.org.<br />
    Isso é exigido pelo <a href="$(HOME)/devel/dmup">DMUP</a>.
  </li>
</ul>

<p>Se os critérios acima não forem mais atendidos em algum momento futuro, o(a)
consultor(a) deverá receber uma mensagem de aviso de que ele(a) está prestes a
ser removido(a) da lista, a menos que cumpra todos os critérios novamente.
Deve haver um período de carência de quatro semanas.</p>

<p>Algumas partes (ou todas) das informações do(a) consultor(a) podem ser
removidas se ele(a) não estiver mais em conformidade com esta política, ou a
critério dos(as) mantenedores(as) da lista. </p>

<h2>Adições, modificações e remoções de entradas</h2>

<p>Se você deseja ser adicionado(a) à lista de consultores(as), envie um email
   para <a href="mailto:consultants@debian.org"> consultants@debian.org </a>,
   em inglês, fornecendo qualquer uma das seguintes informações que você
   gostaria que fosse listada (o endereço de e-mail é obrigatório, todo o resto
   é opcional a seu critério):</p>

<ul>
  <li>País que você quer que seja listado</li>
  <li>Nome</li>
  <li>Empresa</li>
  <li>Endereço</li>
  <li>Telefone</li>
  <li>Fax</li>
  <li>Contato</li>
  <li>E-mail</li>
  <li>URL</li>
  <li>Valores</li>
  <li>Informações adicionais, se tiver alguma</li>
</ul>

<p>Um pedido de atualização das informações do(a) consultor(a) deve ser enviado
para <a href="mailto:consultants@debian.org">consultants@debian.org</a>.
Envie e-mail preferencialmente do endereço de e-mail mencionado na página
dos(as) consultores(as)
(<a href="https://www.debian.org/consultants/">https://www.debian.org/consultants/</a>).</p>

<p>Os(As) consultores(as) podem se comunicar com outros(as) consultores(as)
Debian através da
<a href="https://lists.debian.org/debian-consultants/">lista de discussão debian-consultants</a>.</p>

# tradutores podem, mas não precisam, traduzir o resto - ainda
# BEGIN future version
# fill out the following submission form:</p>
#
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
#
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# New consultant listing submission
# <br />
# <input type="radio" name="submissiontype" value="update">
# Update of an existing consultant listing
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Removal of an existing consultant listing
# </p>
#
# <p>Name:<br />
# <input type="text" name="name" size="50"></p>
#
# <p>Company:<br />
# <input type="text" name="company" size="50"></p>
#
# <p>Address:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Phone:<br />
# <input type="text" name="phone" size="50"></p>
#
# <p>Fax:<br />
# <input type="text" name="fax" size="50"></p>
#
# <p>E-mail:<br />
# <input type="text" name="email" size="50"></p>
#
# <p>URL:<br />
# <input type="text" name="url" size="50"></p>
#
# <p>Rates:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Additional information, if any (<em>in English</em>):<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
#
# </form>
#
# <p>If you are unable to submit the above for any reason whatsoever,
# please send it via e-mail <em>in English</em> to
# END future version
