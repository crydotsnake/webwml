# translation of vote.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <elendil@planet.nl>, 2007, 2008.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: vote.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-01-03 20:43+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Datum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Tijdslijn"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Samenvatting"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominaties"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Terugtrekkingen"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debat"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platforms"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Indiener"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Indiener Voorstel A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Indiener Voorstel B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Indiener Voorstel C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Indiener Voorstel D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Indiener Voorstel E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Indiener Voorstel F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Indiener Voorstel G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Indiener Voorstel H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Steun"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Steun Voorstel A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Steun Voorstel B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Steun Voorstel C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Steun Voorstel D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Steun Voorstel E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Steun Voorstel F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Steun Voorstel G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Steun Voorstel H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Weerstand"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Tekst"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Voorstel A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Voorstel B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Voorstel C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Voorstel D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Voorstel E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Voorstel F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Voorstel G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Voorstel H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Keuzes"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Indiener Amendement"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Steun Amendement"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Tekst Amendement"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Indiener Amendement A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Steun Amendement A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Tekst Amendement A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Indiener Amendement B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Steun Amendement B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Tekst Amendement B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Indiener Amendement C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Steun Amendement C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Tekst Amendement C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Amendementen"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Notulen"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Meerderheidsvereiste"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Data en Statistieken"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimum Discussie"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Stembiljet"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Uitslag"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "In&nbsp;afwachting&nbsp;van&nbsp;sponsors"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Ter&nbsp;discussie"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "In&nbsp;stemming"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Besloten"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Teruggetrokken"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Andere"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Home&nbsp;stempagina"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "How&nbsp;To"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Een&nbsp;voorstel&nbsp;voorleggen"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Een&nbsp;voorstel&nbsp;amenderen"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Een&nbsp;voorstel&nbsp;steunen"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Lees&nbsp;een&nbsp;resultaat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Stem"
