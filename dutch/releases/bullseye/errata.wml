#use wml::debian::template title="Debian 11 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="4ca2253325130f7e96bf2644d31cf5a95fdf7bcc"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor
informatie over eventuele beveiligingsproblemen die in <q>bullseye</q> ontdekt
werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<!-- <ul>
  <li>De eerste tussenrelease, 11.1, werd uitgebracht op
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_bullseye> 11.0 "

<p>Er zijn nog geen tussenreleases voor Debian 11.</p>" "

<p>Zie de <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a>
voor details over wijzigingen tussen 11 en <current_release_bullseye/>.</p>"/>


<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> van elke Debian archief-spiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 11
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Raadpleeg voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
