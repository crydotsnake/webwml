#use wml::debian::template title="Debian 10 -- Σημειώσεις της Έκδοσης" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/buster/release.data"

<if-stable-release release="stretch">
<p>Αυτή είναι μια <strong>υπό επεξεργασία έκδοση</strong> των Σημειώσεων της 
Κυκλοφορίας του Debian 10, με κωδική ονομασία buster, που δεν έχει ακόμα 
κυκλοφορήσει. Οι πληροφορίες που παρουσιάζονται εδώ ίσως να μην είναι ακριβείς 
και παρωχημένες και το πιο πιθανόν ελλειπείς.</p>
</if-stable-release>

<p>Για να βρείτε τι είναι καινούριο στο Debian 10, δείτε τις Σημειώσεις της 
έκδοσης για την αρχιτεκτονική σας:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>Οι Σημειώσεις της Έκδοσης περιέχουν επίσης οδηγίες για χρήστες που 
κάνουν αναβάθμιση από προηγούμενες εκδόσεις.</p>

<p>Αν έχετε ρυθμίσει σωστά την τοπικοποίηση του περιηγητή ιστοσελίδων σας, 
μπορείτε να χρησιμοποιήσετε τον παραπάνω σύνδεσμο για να πάρετε τη σωστή έκδοση 
HTML αυτόματα &mdash; δείτε τη σελίδα <a href="$(HOME)/intro/cn">διαπραγμάτευση 
περιεχομένου</a>. Διαφορετικά, διαλέξτε την ακριβή αρχιτεκτονική, γλώσσα και 
μορφοποίηση που θέλετε από τον πίνακα που ακολουθεί.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Αρχιτεκτονική</strong></th>
  <th align="left"><strong>Μορφοποίηση</strong></th>
  <th align="left"><strong>Γλώσσες</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
