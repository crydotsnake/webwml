#use wml::debian::template title="배포판 아카이브"
#use wml::debian::toc
#use wml::debian::translation-check translation="62a43fd7f1ae2a85c63fc2bc0537f2f09a72f663" maintainer="Sebul"

<toc-display />

<toc-add-entry name="old-archive">debian-archive</toc-add-entry>

<p>
데비안의 오래된 배포판 중 하나에 접속해야 한다면, 그것을 
<a href="http://archive.debian.org/debian/">데비안 아카이브</a> <tt>http://archive.debian.org/debian/</tt>에서 
찾을 수 있습니다.</p>

<!-- FIXME: remove this once the service is setup
<p>You can search packages in the old distributions at <url https://historical.packages.debian.org/>.</p>
     FIXME: remove this once the service is setup -->

<p>릴리스는 dists/ 디렉터리 아래에 코드명에 따라 저장됩니다.</p>
<ul>
  <li><a href="../releases/jessie/">jessie</a> 데비안 8.0</li>
  <li><a href="../releases/wheezy/">wheezy</a> 데비안 7.0</li>
  <li><a href="../releases/squeeze/">squeeze</a> 데비안 6.0</li>
  <li><a href="../releases/lenny/">lenny</a> 데비안 5.0</li>
  <li><a href="../releases/etch/">etch</a> 데비안 4.0</li>
  <li><a href="../releases/sarge/">sarge</a> 데비안 3.1</li>
  <li><a href="../releases/woody/">woody</a> 데비안 3.0</li>
  <li><a href="../releases/potato/">potato</a> 데비안 2.2</li>
  <li><a href="../releases/slink/">slink</a> 데비안 2.1</li>
  <li><a href="../releases/hamm/">hamm</a> 데비안 2.0</li>
  <li>bo   데비안 1.3</li>
  <li>rex  데비안 1.2</li>
  <li>buzz 데비안 1.1</li>
</ul>

<p>
시간이 지남에 따라 이전 릴리스의 이진 패키지가 만료됩니다. 
현재 
<i>squeeze</i>,
<i>lenny</i>,
<i>etch</i>, <i>sarge</i>, <i>woody</i>, <i>potato</i>, <i>slink</i>, <i>hamm</i>
및 <i>bo</i>용 바이너리를 갖고 있으며, 다른 릴리스는 소스 코드만 사용할 수 있습니다.
</p>

<p>APT를 사용한다면 관련 source.list 항목은 다음과 같습니다:</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>또는</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>
다음은 아카이브를 포함하는 미러 목록입니다.
</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">debian-non-US archive</toc-add-entry>

<p>
과거에는 데비안 용으로 패키지된 소프트웨어가 있었는데, 
암호 또는 소프트웨어 특허의 수출 제한으로 인해 
미국(및 기타 국가)에 배포되지 못했습니다. 
데비안은 <q>non-US</q> 아카이브라고 불리는 특별한 아카이브를 유지했습니다.</p>

<p>
이러한 패키지는 데비안 3.1의 메인 아카이브에 통합되었으며, debian-non-US 아카이브는 중단되었습니다. 
실제로 지금은 <em>아카이브되어</em> archive.debian.org 아카이브에 통합되었습니다.</p>

<p>
그것들은 여전히 archive.debian.org 기계에서 이용할 수 있습니다. 
사용 가능한 액세스 방법:</p>
<blockquote><p>
<a href="http://archive.debian.org/debian-non-US/">http://archive.debian.org/debian-non-US/</a><br>
rsync://archive.debian.org/debian-non-US/  (limited)
</p></blockquote>

<p>
APT와 함께 이러한 패키지를 사용하기 위한 관련 source.list 항목:</p>

<pre>
  deb http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
</pre>
