#use wml::debian::template title="데비안 &ldquo;bullseye&rdquo; 설치 정보" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="08a0f9fd40d72266eb96cec4b5e88f09ccc00215" maintainer="Sebul"

<h1>데비안 <current_release_bullseye> 설치</h1>

<if-stable-release release="bookworm">
<p><strong>Debian 11 has been superseded by
<a href="../../bookworm/">Debian 12 (<q>bookworm</q>)</a>. Some of these
installation images may no longer be available, or may no longer work, and
you are recommended to install bookworm instead.
</strong></p>
</if-stable-release>

<if-stable-release release="buster">
<p>
For installation images and documentation about how to install <q>bullseye</q>
(which is currently Testing), see
<a href="$(HOME)/devel/debian-installer/">the Debian-Installer page</a>.
</if-stable-release>

<if-stable-release release="bullseye">
<p>
<strong>데비안을 설치하려면</strong> <current_release_bullseye>
(<em>bullseye</em>), 아래 이미지 중 하나를 받으세요 (모든 i386 및 amd64
CD/DVD 이미지를 USB 스틱에서도 쓸 수 있음):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst CD 이미지 (보통 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>전체 CD 세트</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>전체 DVD 세트</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>다른 이미지 (netboot, flexible usb stick, 등.)</strong></p>
<other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
시스템의 하드웨어에서 장치 드라이버와 함께 <strong>비자유 펌웨어를 로드해야 하면<strong> <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">\일반 펌웨어 패키지의 tarball</a> 중 하나를 사용하거나 이러한 <strong>비자유</strong> 펌웨어를 포함한 <strong>비공식</strong> 이미지를 다운로드할 수 있습니다. tarball 사용 방법과 설치 중 펌웨어 로드에 대한 일반 정보는 <a href="../amd64/ch06s04">설치 안내서</a>에서 확인할 수 있습니다.
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (보통 240-290 MB) <strong>비자유</strong>
CD 이미지 <strong>펌웨어 포함</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>노트</strong>
</p>
<ul>
    <li>
전체 CD 및 DVD 이미지를 다운로드하려면 
BitTorrent 또는 Jigdo를 사용하는 것이 좋습니다.
    </li><li>
덜 일반적인 아키텍처는 제한된 수의 CD 및 DVD 세트 이미지만 
ISO 파일 또는 BitTorrent를 통해 사용할 수 있습니다. 
전체 세트는 jigdo를 통해서만 가능합니다.
    </li><li>
multi-arch <em>CD</em> 이미지 i386/amd64 지원; 
설치는 단일 아키텍처 netinst 이미지에서 설치하는 것과 비슷합니다.
    </li><li>
multi-arch <em>DVD</em> 이미지 i386/amd64 지원; 
설치는 단일 아키텍처 전체 CD 이미지에서 설치하는 것과 비슷합니다.
DVD는 포함한 소스 모두에 대한 소스도 포함합니다.
    </li><li>
설치 이미지는 이미지와 동일한 디렉토리에서 확인 파일(<tt>SHA256SUMS</tt>,<tt>SHA512SUMS</tt> 및 기타)을 사용할 수 있습니다.
    </li>
</ul>


<h1>문서</h1>

<p>
설치 전 <strong>단 하나 문서만</strong> 읽는다면, 
<a href="../i386/apa">Installation Howto</a>, a quick walkthrough of the installation process. 
다른 쓸만한 문서:
</p>

<ul>
<li><a href="../installmanual">Bullseye 설치 안내</a><br />
detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
및 <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
공통 질문과 답</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer 위키</a><br />
커뮤니티가 유지하는 문서</li>
</ul>

<h1 id="errata">정오표</h1>

<p>
데비안 <current_release_bullseye> 과 함께 제공된 설치 관리자에서 알려진 문제 목록입니다. 
데비안을 설치하는 데 문제가 발생했지만 여기에 나열된 문제를 볼 수 없으면 
해당 문제를 설명하는 <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">설치 보고서</a>를 
보내거나 <a href="https://wiki.debian.org/DebianInstaller/BrokenThings">위키</a>에서 
다른 알려진 문제를 확인하십시오.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">릴리스 11.0 정오표</h3>

<dl class="gloss">

     <dt>몇 사운드 카드에 필요한 펌웨어</dt>
     <dd>사운드를 내기 위해서는 펌웨어를 로드해야 하는 사운드 카드가 여러 개 있는 것으로 보입니다. 
     Bullseeye에서는 설치자가 이를 조기에 로드할 수 없기 때문에 설치 시 음성 합성이 불가능합니다. 
     펌웨어가 필요 없는 다른 사운드 카드를 꽂는 것이 해결책입니다. 우리의 노력을 추적하려면 
     <a href="https://bugs.debian.org/992699">umbrella bug report</a>를 참조하십시오. </dd>

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
다음 데비안 릴리스를 위해 개선된 버전의 설치 시스템을 개발하고 있으며 bullseye를 설치하는 데도 사용할 수 있습니다. 
자세한 내용은 <a href="$(HOME)/devel/debian-installer/">Debian-Installer 프로젝트 페이지</a>를 참조하십시오.
</p>
</if-stable-release>
