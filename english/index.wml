#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>The Community</h1>
      <h2>Debian is a Community of People!</h2>
      
#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">People</a></h2>
        <p>Who we are and what we do</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Our Philosophy</a></h2>
        <p>Why we do it, and how we do it</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Get Involved, Contribute</a></h2>
        <p>How you can join us!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">More...</a></h2>
        <p>Additional information about the Debian community</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>The Operating System</h1>
      <h2>Debian is a complete Free Operating System!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Why Debian</a></h2>
        <p>What makes Debian special</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">User Support</a></h2>
        <p>Getting help and documentation</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Security Updates</a></h2>
        <p>Debian Security Advisories (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">More...</a></h2>
        <p>Further links to downloads and software</p>
      </div>
    </div>
  </div>
</div>

<hr>

<!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <div class="row">
#   <div class="column styled-href-blue column-left">
#    <div style="text-align: center">
#      <h2><a href="https://debconf21.debconf.org/">DebConf21</a> is underway!</h2>
#      <p>The Debian Conference is being held online from August 24 to August 28, 2021.</p>
#    </div>
#  </div>
# </div>

<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>News and Announcements about Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">All the news</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="security/dsa-long">
:#rss#}

