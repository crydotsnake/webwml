<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jon Franklin and Pawel Wieczorkiewicz found an issue in the ISC DHCP
client and server when parsing lease information, which could lead to
denial of service via application crash.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.3.5-3+deb9u2.</p>

<p>We recommend that you upgrade your isc-dhcp packages.</p>

<p>For the detailed security status of isc-dhcp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/isc-dhcp">https://security-tracker.debian.org/tracker/isc-dhcp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2674.data"
# $Id: $
