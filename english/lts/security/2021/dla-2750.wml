<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Exiv2, a C++ library
and a command line utility to manage image metadata which could result
in denial of service or the execution of arbitrary code if a malformed
file is parsed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
0.25-3.1+deb9u3.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>For the detailed security status of exiv2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/exiv2">https://security-tracker.debian.org/tracker/exiv2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2750.data"
# $Id: $
