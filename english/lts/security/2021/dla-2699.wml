<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in ipmitool, an utility for IPMI control with
kernel driver or LAN interface.
Neglecting proper checking of input data might result in buffer overflows
and possible remote code execution.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.8.18-3+deb9u1.</p>

<p>We recommend that you upgrade your ipmitool packages.</p>

<p>For the detailed security status of ipmitool please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ipmitool">https://security-tracker.debian.org/tracker/ipmitool</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2699.data"
# $Id: $
