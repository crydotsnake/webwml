<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13390">CVE-2019-13390</a>

    <p>rawenc: Only accept the appropriate stream type for raw muxers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17542">CVE-2019-17542</a>

    <p>Heap-based buffer overflow in vqa_decode_chunk.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13904">CVE-2020-13904</a>

    <p>Use-after-free via a crafted EXTINF duration
       in an m3u8 file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7:3.2.15-0+deb9u1.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2291.data"
# $Id: $
