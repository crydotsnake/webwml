<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue was discovered in LinuxTV xawtv before 3.107. The function
dev_open() in v4l-conf.c does not perform sufficient checks to
prevent an unprivileged caller of the program from opening unintended
filesystem paths. This allows a local attacker with access to the
v4l-conf setuid-root program to test for the existence of arbitrary
files and to trigger an open on arbitrary files with mode O_RDWR.
To achieve this, relative path components need to be added to the
device path, as demonstrated by a
v4l-conf -c /dev/../root/.bash_history command.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.103-3+deb8u1.</p>

<p>We recommend that you upgrade your xawtv packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2246.data"
# $Id: $
