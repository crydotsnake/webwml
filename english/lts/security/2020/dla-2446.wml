<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities were discovered in moin, a Python clone of WikiWiki.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15275">CVE-2020-15275</a>

     <p>Catarina Leite discovered that moin is prone to a stored XSS
     vulnerability via SVG attachments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25074">CVE-2020-25074</a>

     <p>Michael Chapman discovered that moin is prone to a remote code
     execution vulnerability via the cache action.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.9.9-1+deb9u2.</p>

<p>We recommend that you upgrade your moin packages.</p>

<p>For the detailed security status of moin please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/moin">https://security-tracker.debian.org/tracker/moin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2446.data"
# $Id: $
