<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Todd Carson discovered some integer overflows in libX11, which could
lead to heap corruption when processing crafted messages from an input
method.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:1.6.4-3+deb9u2.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>For the detailed security status of libx11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libx11">https://security-tracker.debian.org/tracker/libx11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2312.data"
# $Id: $
