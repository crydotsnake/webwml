<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Adith Sudhakar discovered a cross-site request forgery (CSRF) issue in
monit, a utility for monitoring hosts and services. An attacker could
cause an authenticated admin to change monitoring for hosts/services
through a forged link. This update fixes the vulnerability by adding
CSRF protection via a security token and enforced POST requests for
actions that cause changes to the monitoring.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4-2+deb7u1.</p>

<p>We recommend that you upgrade your monit packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-732.data"
# $Id: $
